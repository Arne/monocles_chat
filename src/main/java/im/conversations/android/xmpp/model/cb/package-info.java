@XmlPackage(namespace = Namespace.CHANNEL_BINDING)
package im.conversations.android.xmpp.model.cb;

import eu.siacs.conversations.xml.Namespace;
import im.conversations.android.annotation.XmlPackage;
