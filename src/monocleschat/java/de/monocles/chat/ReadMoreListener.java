package de.monocles.chat;

/**
 * Created by Mays Atari.
 */

public interface ReadMoreListener {
    void onReadMoreClick(boolean readMore);
}